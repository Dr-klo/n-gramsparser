﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace N_grams
{
   class NGram
    {
        public string Gram { get; set; }
        public long Count { get; set; }
    }
    class NGrams
    {
        public List<string> Words { get; set; } 
        public List<NGram> One { get; set; }
        public List<NGram> Two { get; set; }
        public List<NGram> Three { get; set; }
        public List<NGram> Four { get; set; }

        public NGrams()
        {
            Words=new List<string>();
            One = new List<NGram>();
            Two = new List<NGram>();
            Three = new List<NGram>();
            Four = new List<NGram>();
        }
        public void Sort()
        {
            One = One.OrderByDescending(x => x.Count).ToList();
            Two = Two.OrderByDescending(x => x.Count).ToList();
            Three = Three.OrderByDescending(x => x.Count).ToList();
            Four = Four.OrderByDescending(x => x.Count).ToList();
        }
    }
    
    public class NGramResolver
    {
        private NGrams NGrams = new NGrams();
        private void Parse(int n,string word, List<NGram> Ngrams)
        {
            if (word.Length > n)
            {
                for (var i = 0; i <= word.Length -n; i++)
                {
                    var gram = word.Substring(i, n);
                    if (Ngrams.Any(x => x.Gram == gram)) Ngrams.First(x => x.Gram == gram).Count++;
                    else
                    {
                        Ngrams.Add(new NGram() { Gram = gram, Count = 1 });
                    }
                }
            }
        }
        private void ParseGramm(string word)
        {
            var N = 1;
            Parse(N,word,NGrams.One);
            word = " " + word + " ";
            Parse(++N, word, NGrams.Two);
            Parse(++N, word, NGrams.Three);
            Parse(++N, word, NGrams.Four);
            
        }
        private void SaveWords(List<string> words)
        {
            using (var file = new System.IO.StreamWriter("Words.txt"))
            {
                foreach (var word in words)
                {
                    file.WriteLine(word);
                }
                file.Flush();
            }


        }
            
        private void Save(List<NGram> NGrams, int max, string description)
        {

            using (var file = new System.IO.StreamWriter(description+".txt"))
            {
                
                var k = max > NGrams.Count ? NGrams.Count : max;
                for (int index = 0; index <k  ; index++)
                {
                    var nGram = NGrams[index];
                    file.WriteLine(string.Format("{0},{1}", nGram.Gram, nGram.Count));
                }
                file.Flush();
            }
        }
        public NGramResolver(string text)
        {
            var pattern = @"[\u00C0-\u1FFF\u2C00-\uD7FF\w]+";

            Regex rgx = new Regex(pattern);
            while (rgx.Match(text).Success)
            {
                Match m = rgx.Match(text);
                if (m.Success)
                {

                    string word = text.Substring(m.Index, m.Length).ToLower();
                    text = text.Substring(m.Index + m.Length);
                    //Console.WriteLine(word);
                    ParseGramm(word);
                    NGrams.Words.Add(word);
                }

            }
          
            NGrams.Sort();
            Save(NGrams.Four, 1000, "Four_grams");
            Save(NGrams.Three, 1000, "Three_grams");
            Save(NGrams.Two, 1000, "Two_grams");
            Save(NGrams.One, 1000, "One_grams"); 
            SaveWords(NGrams.Words);
            
        }
    }

    class Program
    {


        static void Main(string[] args)
        {  
            Console.WriteLine("Write the textfile name");
            var filename = Console.ReadLine();
            string text = "";
            //filename = "1.txt";

            try
            {
                text = System.IO.File.ReadAllText(@"" + filename);
                var tmp = new NGramResolver(text);


            }
            catch (Exception e)
            {
                Console.WriteLine("Error:" + e);
            }

            Console.WriteLine("-END-");
          
        }
    }
}
