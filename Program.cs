﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;



//Created by Dr.klo (Dec.Mac.klo@Gmail.com
//NGram parser betta

namespace N_grams
{
   class NGram
    {
        public string Gram { get; set; }
        public long Count { get; set; }
    }
    class NGrams
    {
        public List<string> Words { get; set; } 
        public List<NGram> One { get; set; }
        public List<NGram> Two { get; set; }
        public List<NGram> Three { get; set; }
        public List<NGram> Four { get; set; }

        public NGrams()
        {
            Words=new List<string>();
            One = new List<NGram>();
            Two = new List<NGram>();
            Three = new List<NGram>();
            Four = new List<NGram>();
        }
        public void Sort()
        {
            One = One.OrderByDescending(x => x.Count).ToList();
            Two = Two.OrderByDescending(x => x.Count).ToList();
            Three = Three.OrderByDescending(x => x.Count).ToList();
            Four = Four.OrderByDescending(x => x.Count).ToList();
        }
    }
    
    public class NGramResolver
    {
        private NGrams NGrams = new NGrams();
        private void Parse(int n,string word, List<NGram> Ngrams)
        {
            if (word.Length > n)
            {
                for (var i = 0; i <= word.Length -n; i++)
                {
                    var gram = word.Substring(i, n);
                    if (Ngrams.Any(x => x.Gram == gram)) Ngrams.First(x => x.Gram == gram).Count++;
                    else
                    {
                        Ngrams.Add(new NGram() { Gram = gram, Count = 1 });
                    }
                }
            }
        }
        private void ParseGramm(string word)
        {
            var N = 1;
            Parse(N,word,NGrams.One);
            word = " " + word + " ";
            Parse(++N, word, NGrams.Two);
            Parse(++N, word, NGrams.Three);
            Parse(++N, word, NGrams.Four);
            
        }
        private void SaveWords(List<string> words)
        {
            using (var file = new System.IO.StreamWriter("Words.txt"))
            {
                foreach (var word in words)
                {
                    file.WriteLine(word);
                }
                file.Flush();
            }


        }
            
        private void Save(List<NGram> NGrams, int max, string description)
        {

            using (var file = new System.IO.StreamWriter(description+".txt"))
            {
                //if (max < 1) max = NGrams.Count;
                var k = max > NGrams.Count ? NGrams.Count : max<1?NGrams.Count:max;
                for (int index = 0; index <k  ; index++)
                {
                    var nGram = NGrams[index];
                    file.WriteLine(string.Format("{0},{1}", nGram.Gram, nGram.Count));
                }
                file.Flush();
            }
        }
        public NGramResolver(string text,int count,int length)
        {
            
            var pattern = @"[\u00C0-\u1FFF\u2C00-\uD7FF\w]+";
            var totallength = text.Length;
            int progress = 0;
            var previousprogress = progress;
            Regex rgx = new Regex(pattern);
            while (rgx.Match(text).Success)
            {
                Match m = rgx.Match(text);
                string word = text.Substring(m.Index, m.Length).ToLower();
                text = text.Substring(m.Index + m.Length);
                if (word.Length >= length)
                {
                    progress = 100 - text.Length * 100 / totallength; //prgress bar
                    if (progress > previousprogress) { Console.WriteLine("Progress: {0}%", progress); previousprogress = progress; }
                    ParseGramm(word);
                    NGrams.Words.Add(word);
                }

            }
          
            NGrams.Sort();
            Save(NGrams.Four, count, "Four_grams");
            Save(NGrams.Three, count, "Three_grams");
            Save(NGrams.Two, count, "Two_grams");
            Save(NGrams.One, count, "One_grams"); 
            SaveWords(NGrams.Words);
            
        }
    }

    class Program
    {


        static void Main(string[] args)
        {  
            Console.WriteLine("Write the textfile name (only UTF-8 encoding)");
            var filename = Console.ReadLine();
            string text = "";
            Console.WriteLine("Input the N-Grams Count (of each N-Gram). '-1' is all n-gram");
            var _count = Console.ReadLine();
            int count=1000;
            if(!Int32.TryParse(_count,out count)) {Console.WriteLine("Wrong count,default value is set 1000");}
            Console.WriteLine("Input the minimal word length . '-1' is all words");
            var _length = Console.ReadLine();
            int length = 3;
            if (!Int32.TryParse(_length, out length)) { Console.WriteLine("Wrong length,default value is set 3"); }
            //filename = "1.txt";

            try
            {
                text = System.IO.File.ReadAllText(@"" + filename);
                var tmp = new NGramResolver(text,count,length);


            }
            catch (Exception e)
            {
                Console.WriteLine("Error:" + e);
            }

            Console.WriteLine("-END-");
          
        }
    }
}
